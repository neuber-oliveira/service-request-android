package com.neuberdesigns.ServiceRequest;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONTokener;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import br.com.dopaminamob.desbrava.BuildConfig;
import br.com.dopaminamob.desbrava.R;

import static com.google.android.gms.internal.zzhl.runOnUiThread;

/**
 * Created by neuber on 13/04/14. -23.6834955 -46.67099403
 */
public abstract class ServiceRequest extends AsyncTask<String, Void, Object>{
	public static final String LOG_ID = "service_request";
	public static final String METHOD_GET = "GET";
	public static final String METHOD_POST = "POST";
	public static final String JSON_RESPONSE_STATE = "json_response_state";
	public static int MAX_BUFFER_SIZE = 1 * 1024 * 1024;
	private static final int READ_TIME_OUT = 120000;
	private static final int CONNECTION_TIME_OUT = 120000;
	
	protected Bundle savedInstanceState = new Bundle();
	protected HttpURLConnection conn;
	protected OutputStream outputStream;
	protected String serverResponse = "";
	protected String fullRequest 	= "";
	protected final String crlf 	= "\r\n";
	protected String twoHyphens 	= "--";
	protected String boundary 		= "NdServiceRequestBoundary"+String.valueOf( System.currentTimeMillis() );
	
	protected String baseURL 		= "";
	protected String token 			= "";
	protected boolean closeOnFinish = true;

	protected Context context;
	protected ProgressDialog progressDialog;
	protected ServiceRequest.CustomLoader customLoader;
	protected FragmentManager fragmentManager;
	
	protected HashMap<String, String> params = new HashMap<>();
	protected HashMap<String, List<String>> paramsMulti = new HashMap<>();
	protected HashMap<String, FileUploadHolder> uploadSingle = new HashMap<>();
	protected HashMap<String, List<FileUploadHolder>> uploadMulti = new HashMap<>();
	
	protected List<String> parametersList = new ArrayList<>();
	protected List<String> uploadList = new ArrayList<>();
	
	protected ServiceRequestCallbackInterface listener;
	
	public ServiceRequest(Context context) {
		init(context, null);
	}
	
	public ServiceRequest(Context context, ServiceRequestCallbackInterface srci) {
		init(context, srci);
	}
	
	protected void init(Context ctx, ServiceRequestCallbackInterface srci){
		this.context = ctx;
		this.listener = srci;
	}
	
	protected Object restoreState(){
		Object jsonResponse = null;
		if( !serverResponse.isEmpty() ){
			jsonResponse = parseResponse(serverResponse);
			if(listener != null)
				listener.updateUI(jsonResponse);
		}
		
		return jsonResponse;
	}
	
	public void restoreInstanceState(Bundle state){
		if( state != null ){
			serverResponse = state.getString(JSON_RESPONSE_STATE, "");
		}
	}
	
	public void saveInstanceState(Bundle state){
		state.putCharSequence( JSON_RESPONSE_STATE, getServerResponse() );
	}
	
	public Object connect(String endpoint, String method) throws IOException {
		return sendToUrl(endpoint, method);
	}

	public Object sendToUrl(String endpoint, String method) throws IOException {
		InputStream inputStream = null;
		outputStream = null;
		String fullUrl;
		String response = null;
		Object jsonResponse = restoreState();
		URL url;
		
		//boolean isGet = isGet( method );
		boolean isPost = isPost( method );
		
		//return from saved instace :)
		if( jsonResponse!=null )
			return jsonResponse;
		
		buildParamsList();
		
		fullUrl = baseURL+endpoint+buildQueryString( method );
		url = new URL(fullUrl);
		conn = (HttpURLConnection) url.openConnection();
		conn.setRequestProperty("Connection", "Keep-Alive");
		conn.setRequestProperty("Cache-Control", "no-cache");
		conn.setReadTimeout( READ_TIME_OUT );
		conn.setConnectTimeout( CONNECTION_TIME_OUT );
		conn.setDoInput( true );
		conn.setRequestMethod( method );
		//conn.setChunkedStreamingMode( 512 );
		
		if( isPost ){
			//conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
			
			outputStream = conn.getOutputStream();
			
			buildPostString();
			writeBoundarieEnd();
		}
		
		try {
			conn.connect();
			inputStream = conn.getInputStream();
			serverResponse = getStringResponse(inputStream);
			jsonResponse = parseResponse(serverResponse);
			
		} catch( OutOfMemoryError oomExeption ){
			Log.d(LOG_ID, "OuOfMemory: " + oomExeption.getMessage());
			Toast.makeText( context, "Ops! Out off memory", Toast.LENGTH_SHORT ).show();
			oomExeption.printStackTrace();
		} catch( FileNotFoundException notFoundException ){
			notFoundException.printStackTrace();
			
		} catch( EOFException eofException ){
			eofException.printStackTrace();
			
		} catch( IOException ioException ){
			Log.d(LOG_ID, "IO Exception: " + ioException.getMessage());
			ioException.printStackTrace();
		} finally{}
		
		if( isPost && outputStream!=null ){
			outputStream.flush();
			outputStream.close();
		}
		
		if( inputStream != null ){
			inputStream.close();
		}
		
		if( BuildConfig.DEBUG ){
			Log.d(LOG_ID, "Request to: ["+method+"] "+fullUrl);
			Log.d(LOG_ID, "The response code is: " + conn.getResponseCode());
			Log.d(LOG_ID, "The response raw: "+serverResponse);
			Log.d(LOG_ID, "Parameters: "+TextUtils.join("&", parametersList) );
			Log.d(LOG_ID, "Uploads: "+TextUtils.join("&", uploadList) );
			Log.d(LOG_ID, "Full Request: \n"+fullRequest );
		}
		
		return jsonResponse;
	}
	
	
	protected List<String> buildParamsList(){
		Iterator itSingle 	= params.entrySet().iterator();
		Iterator itMulti 	= paramsMulti.entrySet().iterator();
		
		while( itSingle.hasNext() ){
			Map.Entry pair = (Map.Entry)itSingle.next();
			String key = pair.getKey()+"="+pair.getValue();
			Log.d(LOG_ID, "PARAM: " + key);
			parametersList.add( key );
			//itSingle.remove(); // avoids a ConcurrentModificationException
		}
		
		while (itMulti.hasNext()) {
			Map.Entry pair = (Map.Entry)itMulti.next();
			List<String> curList = (List<String>)pair.getValue();
			String key;
			
			for( String value : curList ){
				key = pair.getKey()+"[]="+value;
				parametersList.add( key );
			}
			
			Log.d(LOG_ID, "PARAM: "+pair.getKey()+"[]="+TextUtils.join(",", curList) );
			//itMulti.remove(); // avoids a ConcurrentModificationException
		}
		
		return parametersList;
	}
	
	
	protected String buildQueryString(String method){
		boolean hasToken = false;
		String queryString = "";
		String parametersProperty = TextUtils.join("&", parametersList);
		
		if( (token!=null && !token.isEmpty()) ){
			queryString +=  "?token="+token;
			hasToken = true;
		}
		
		if( isGet( method ) ){
			queryString += (hasToken ? "&" : "?")+parametersProperty;
		}
		
		return queryString;
	}
	
	protected void buildPostString() throws IOException{
		writeParamText();
		writeParamFile();
	}
	
	protected void writeParamText() throws IOException{
		Iterator itSingle 	= params.entrySet().iterator();
		Iterator itMulti 	= paramsMulti.entrySet().iterator();
		
		while( itSingle.hasNext() ){
			Map.Entry pair = (Map.Entry)itSingle.next();
			String key = (String)pair.getKey();
			String value = (String)pair.getValue();
			
			writePostParam(key, value, false );
			
			itSingle.remove(); // avoids a ConcurrentModificationException
		}
		
		while (itMulti.hasNext()) {
			Map.Entry pair = (Map.Entry)itMulti.next();
			List<String> curList = (List<String>)pair.getValue();
			String key;
			
			for( String value : curList ){
				key = (String)pair.getKey();
				writePostParam( key, value, true );
			}
			itMulti.remove(); // avoids a ConcurrentModificationException
		}
	}
	
	protected void writeParamFile() throws IOException{
		Iterator itSingle 	= uploadSingle.entrySet().iterator();
		Iterator itMulti 	= uploadMulti.entrySet().iterator();
		boolean compress 	= false;
		
		while( itSingle.hasNext() ){
			Map.Entry pair = (Map.Entry)itSingle.next();
			String key = (String)pair.getKey();
			
			FileUploadHolder upload = (FileUploadHolder)pair.getValue();
			String param = "Content-Disposition: form-data; name=\"" + key + "\"; filename=\"" + upload.getFilename()+"\""+crlf+"Content-type: image/jpeg"+crlf+crlf;
			
			writeBoundarie();
			fullRequest += param;
			conn.getOutputStream().write( param.getBytes() );
			writeFile( upload.getFilename() );
			
			fullRequest += compress+crlf;
			
			itSingle.remove(); // avoids a ConcurrentModificationException
		}
		
		
		while (itMulti.hasNext()) {
			Map.Entry pair = (Map.Entry)itMulti.next();
			String key = (String)pair.getKey();
			List<FileUploadHolder> uploads = (List<FileUploadHolder>)pair.getValue();
			String param;
			
			for( FileUploadHolder upload : uploads ){
				param = "Content-Disposition: form-data; name=\"" + key + "[]\"; filename=\"" + upload.getFilename() + "\""+crlf+"Content-type: image/jpeg"+crlf+crlf;
				
				writeBoundarie();
				fullRequest += param;
				conn.getOutputStream().write( param.getBytes() );
				writeFile(upload.getFilename());
				
				fullRequest += compress+crlf;
			}
			
			itMulti.remove(); // avoids a ConcurrentModificationException
		}
	}
	
	public void writePostParam(String key, String value, boolean isArray) throws IOException{
		writeBoundarie();
		String param = "Content-Disposition: form-data; name=\"" + key + ( isArray ? "[]" : "" ) + "\"" + crlf+crlf+ value+crlf;
		fullRequest += param;
		conn.getOutputStream().write( param.getBytes("UTF-8") );
		//outputStream.writeBytes(param);
		
		conn.getOutputStream().flush();
	}
	
	public void writeBoundarie() throws IOException{
		String bound = twoHyphens + boundary + crlf;
		fullRequest += bound;
		conn.getOutputStream().write(bound.getBytes());
	}
	
	public void writeBoundarieEnd() throws IOException{
		String bound = twoHyphens + boundary+twoHyphens;
		fullRequest += bound;
		conn.getOutputStream().write(bound.getBytes());
	}
	
	public void writeFile(String path) throws IOException{
		InputStream fileInputStream = null;
		File file = new File(path);
		boolean compress 	= false;
		Bitmap bitmap = BitmapFactory.decodeFile(path);
		
		try{
			/*fileInputStream = new BufferedInputStream(new FileInputStream(file));
			// create a buffer of maximum size
			int bytesAvailable = fileInputStream.available();
			
			int maxBufferSize = 2048;
			int bufferSize = Math.min(bytesAvailable, maxBufferSize);
			byte[] buffer = new byte[bufferSize];
			byte byt[] = new byte[bufferSize];
			
			int bytesRead;
			while ((bytesRead = fileInputStream.read(buffer)) > 0) {
				conn.getOutputStream().write(buffer, 0, bytesRead);
			}*/
			
			compress = bitmap.compress(Bitmap.CompressFormat.JPEG, 80, conn.getOutputStream() );
			
			if( compress ){
				bitmap.recycle();
				bitmap = null;
			}
			
			conn.getOutputStream().write(crlf.getBytes());
			conn.getOutputStream().flush();
		}finally{
			if(fileInputStream != null){
				fileInputStream.close();
			}
		}
	}
	
	public String getServerResponse(){
		return serverResponse;
	}
	
	
	
	
	protected boolean isGet(String m){
		return m.equals( METHOD_GET );
	}
	
	protected boolean isPost(String m){
		return m.equals( METHOD_POST );
	}
	
	public void setBaseURL(String url){
		baseURL = url;
	}
	
	public void setCallback(ServiceRequestCallbackInterface srci){
		listener = srci;
	}
	
	
	public void setCustomLoader(CustomLoader customLoader, FragmentManager manager){
		this.fragmentManager = manager;
		this.customLoader = customLoader;
	}
	
	public void setCloseOnFinish(boolean closeOnFinish){
		this.closeOnFinish = closeOnFinish;
	}
	
	public ProgressDialog getLoader(){
		return progressDialog;
	}
	
	public ServiceRequest.CustomLoader getCustomLoader(){
		return customLoader;
	}
	
	public boolean checkConnection(){
		ConnectivityManager connMan = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMan.getActiveNetworkInfo();
		
		return networkInfo != null && networkInfo.isConnected();
	}
	
	public void addParam(String key, String value){
		params.put(key, value);
	}
	
	public void addParam(String key, List<String> values){
		paramsMulti.put(key, values);
	}
	
	public void addUpload( String key, FileUploadHolder upload ){
		uploadSingle.put( key, upload );
	}
	
	public void addUpload( String key, List<FileUploadHolder> uploads ){
		uploadMulti.put(key, uploads);
	}
	
	protected String getStringResponse(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is) );
		StringBuilder sb = new StringBuilder();
		String line;

		while ( (line = br.readLine()) != null) {
			sb.append( line ).append( "\n" );
		}
		br.close();
		
		return sb.toString();
	}

	protected Object parseResponse(String response){
		Object json = null;
		try {
			json = new JSONTokener(response).nextValue();
			
		} catch (JSONException e) {
			e.getMessage();
			//e.printStackTrace();
		}

		return json;
	}
	
	public void setToken(String token){
		this.token = token;
	}
	
	public static boolean isConnected(Context context){
		ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
		
		return isConnected;
	}
	
	protected void displayNoConnectionDialog(){
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		alert.setTitle(R.string.no_connection_title);
		alert.setMessage(R.string.no_connection_message);
		
		alert.show();
	}
	
	//Show allert message
	public void showAlert(){
		String message = context.getString( R.string.service_request_default_alert_message );
		String title = context.getString( R.string.service_request_title );
		//showAlert( message );
		showCustomAlert(message, title);
	}

	public void showAlert(String message){
		String title = context.getString(R.string.service_request_title);
		showCustomAlert(message, title);
	}

	public void showCustomAlert(String message, String title){
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message);

		if( title!=null ){
			builder.setTitle(title);
		}

		builder.setPositiveButton(R.string.service_request_button_ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});

		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	//Async task operations
	@Override
	protected void onPreExecute(){
		String progressTitle = context.getResources().getString(R.string.service_request_progress_title);
		String progressMessage = context.getResources().getString(R.string.service_request_progress_message);
		
		
			if( customLoader == null ){
				progressDialog = ProgressDialog.show(context, progressTitle, progressMessage, true, false);
			}else{
				customLoader.displayLoader( fragmentManager );
			}
			
			
	}

	@Override
	protected Object doInBackground(String... params) {
		Object json = null;
		String endpoint = params[0];
		String method = ( params.length>1 ) ? params[1] : METHOD_GET;
		
		
		try {
			json = connect( endpoint, method );
			
			/*if( isConnected(context) ){
			}else{
				runOnUiThread(new Runnable(){
					public void run(){
						displayNoConnectionDialog();
					}
				});
			}*/
		} catch (IOException e) {
			e.printStackTrace();
			Log.e( LOG_ID, "Could no connect to the server: ["+method+"] "+baseURL+endpoint );
			
			for( String param : params ){
				Log.d( LOG_ID, param );
			}
		}

		return json;
	}

	protected void onPostExecute(Object json){
		if( closeOnFinish ){
			if(customLoader == null){
				progressDialog.dismiss();
			} else{
				customLoader.dismissLoader();
			}
		}
		
		if( listener!=null && json!=null )
			listener.updateUI(json);
	}
	
	
	public interface CustomLoader {
		void displayLoader(FragmentManager manager);
		void dismissLoader();
		String getLoaderTag();
	}
}
