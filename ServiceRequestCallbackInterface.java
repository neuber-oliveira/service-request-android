package com.neuberdesigns.ServiceRequest;

/**
 * Created by neuber on 14/04/14.
 */
public interface ServiceRequestCallbackInterface {
	public void updateUI(Object response);
}
